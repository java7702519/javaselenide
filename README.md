# JavaSelenide



## About

This is an implementation of a sample test automation using the Java+Selenide framework. This is intended to demonstrate the features and to provide a working sample of the framework.

The sample application under test is the [Swag Labs](https://www.saucedemo.com/) online e-commerce platform.

## Getting started

### Pre-requisites

The following should be available on the host running the code:
* Java 17
* Gradle 8.0.2

### Running the test
1. Clone the project
2. Run the sample TestNG suite `src/suites/SwagLabs.xml`

### Reporting
After running the test, an HTML report will be generated in `results` directory.

You can see a sample report in [JavaSelenideSampleReport](https://gitlab.com/java7702519/javaselenidesamplereport) Gitlab project. Please note that you may need to clone or download the project to view the HTML report including images and attachments.