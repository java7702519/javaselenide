package swaglabs;

import definitions.projects.SwagLabsTest;
import org.testng.annotations.Test;
import swaglabs.products.Product;
import swaglabs.products.ProductStates;
import swaglabs.sidebar.Sidebar;

import java.util.List;

public class TestCheckout extends SwagLabsTest {
    @Test
    public void testFailCheckoutMissingInfo() {
        steps.swagLabs.login.as("standard_user", "secret_sauce");

        steps.swagLabs.header.verifyPageDisplay("Products");
        steps.swagLabs.products.addProductsToCart(3);

        steps.swagLabs.header.openCart();
        steps.swagLabs.header.verifyPageDisplay("Your Cart");
        steps.swagLabs.cart.verifyProducts();
        steps.swagLabs.cart.checkout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Your Information");

        steps.swagLabs.yourInfo.enterInfo("", "Tan", "123");
        steps.swagLabs.yourInfo.continueCheckout();
        steps.swagLabs.yourInfo.verifyErrorMessage();

        steps.swagLabs.header.openCart();
        steps.swagLabs.header.verifyPageDisplay("Your Cart");
        steps.swagLabs.cart.verifyProducts();
        steps.swagLabs.cart.checkout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Your Information");

        steps.swagLabs.yourInfo.enterInfo("Richard", "", "123");
        steps.swagLabs.yourInfo.continueCheckout();
        steps.swagLabs.yourInfo.verifyErrorMessage();

        steps.swagLabs.header.openCart();
        steps.swagLabs.header.verifyPageDisplay("Your Cart");
        steps.swagLabs.cart.verifyProducts();
        steps.swagLabs.cart.checkout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Your Information");

        steps.swagLabs.yourInfo.enterInfo("Richard", "Tan", "");
        steps.swagLabs.yourInfo.continueCheckout();
        steps.swagLabs.yourInfo.verifyErrorMessage();
    }

    @Test
    public void testContinueCheckoutAfterLogout() {
        steps.swagLabs.login.as("standard_user", "secret_sauce");

        steps.swagLabs.header.verifyPageDisplay("Products");
        List<Product> products = steps.swagLabs.products.addProductsToCart(3);

        steps.swagLabs.sidebar.navigateTo(Sidebar.NavLinks.LOGOUT);

        steps.swagLabs.login.as("standard_user", "secret_sauce");
        products.forEach(product -> steps.swagLabs.products.verifyProductStateAs(product, ProductStates.State.ADDED));

        steps.swagLabs.header.openCart();
        steps.swagLabs.header.verifyPageDisplay("Your Cart");
        steps.swagLabs.cart.verifyProducts();
        steps.swagLabs.cart.checkout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Your Information");
        steps.swagLabs.yourInfo.enterInfo("first name", "last name", "123");
        steps.swagLabs.yourInfo.continueCheckout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Overview");
        steps.swagLabs.overview.verifyProducts();
        steps.swagLabs.overview.verifyItemsTotal();
        steps.swagLabs.overview.verifyTotal();
        steps.swagLabs.overview.complete();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Complete!");
        steps.swagLabs.complete.verifyConfirmationText();

        steps.swagLabs.sidebar.navigateTo(Sidebar.NavLinks.LOGOUT);

    }

    @Test
    public void testSuccessfulCheckout() {
        steps.swagLabs.login.as("standard_user", "secret_sauce");

        steps.swagLabs.header.verifyPageDisplay("Products");
        steps.swagLabs.products.addProductsToCart(3);

        steps.swagLabs.header.openCart();
        steps.swagLabs.header.verifyPageDisplay("Your Cart");
        steps.swagLabs.cart.verifyProducts();
        steps.swagLabs.cart.checkout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Your Information");
        steps.swagLabs.yourInfo.enterInfo("first name", "last name", "123");
        steps.swagLabs.yourInfo.verifyErrorMessage();
        steps.swagLabs.yourInfo.continueCheckout();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Overview");
        steps.swagLabs.overview.verifyProducts();
        steps.swagLabs.overview.verifyItemsTotal();
        steps.swagLabs.overview.verifyTotal();
        steps.swagLabs.overview.complete();

        steps.swagLabs.header.verifyPageDisplay("Checkout: Complete!");
        steps.swagLabs.complete.verifyConfirmationText();

        steps.swagLabs.sidebar.navigateTo(Sidebar.NavLinks.LOGOUT);
    }
}
