package swaglabs;

import definitions.projects.SwagLabsTest;
import org.testng.annotations.Test;
import swaglabs.products.Product;
import swaglabs.products.ProductStates;
import swaglabs.products.Sort;
import swaglabs.sidebar.Sidebar;

import java.util.List;

public class TestProducts extends SwagLabsTest {
    @Test
    public void testSorting() {
        login();
        steps.swagLabs.products.verifySorting(Sort.SortOptions.NAME_A_TO_Z);
        steps.swagLabs.products.verifySorting(Sort.SortOptions.NAME_Z_TO_A);
        steps.swagLabs.products.verifySorting(Sort.SortOptions.PRICE_LOW_TO_HIGH);
        steps.swagLabs.products.verifySorting(Sort.SortOptions.PRICE_HIGH_TO_LOW);
    }

    @Test
    public void testCart() {
        login();
        steps.swagLabs.products.verifyAddingAndRemovingToCart();
    }

    @Test
    public void testResetProductsState() {
        login();
        steps.swagLabs.products.addProductsToCart(3);
        steps.swagLabs.products.verifyResetAppState();
    }

    @Test
    public void testCartStateAfterLogout() {
        login();
        List<Product> products = steps.swagLabs.products.addProductsToCart(2);
        steps.swagLabs.sidebar.navigateTo(Sidebar.NavLinks.LOGOUT);

        login();
        products.forEach(product -> steps.swagLabs.products.verifyProductStateAs(product, ProductStates.State.ADDED));
        steps.swagLabs.sidebar.navigateTo(Sidebar.NavLinks.RESET_APP_STATE);
    }

    private void login() {
        steps.swagLabs.login.as("standard_user", "secret_sauce");
        steps.swagLabs.login.verifySuccessfulLogin();
    }
}
