package swaglabs;

import definitions.projects.SwagLabsTest;
import org.testng.annotations.Test;

public class TestLogin extends SwagLabsTest {
    @Test
    public void testMissingUsername() {
        steps.swagLabs.login.as("", "password");
        steps.swagLabs.login.verifyMissingUsername();
    }

    @Test
    public void testMissingPassword() {
        steps.swagLabs.login.as("standard_user", "");
        steps.swagLabs.login.verifyMissingPassword();
    }

    @Test
    public void testInvalidCredentials() {
        steps.swagLabs.login.as("incorrect_user", "incorrect_password");
        steps.swagLabs.login.verifyInvalidCredentials();
    }
}
