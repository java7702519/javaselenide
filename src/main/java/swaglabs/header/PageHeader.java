package swaglabs.header;

import com.codeborne.selenide.Condition;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

public class PageHeader extends Page {
    public PageHeader(ProjectApp app) {
        super(app);
    }

    public String getCurrentPageTitle() {
        Log.info("Getting current page display");
        field("appLogoText").shouldBe(Condition.visible);
        return field("pageTitle").getText();
    }

    public void openCart() {
        field("cart").click();
    }

    public int getCartSize() {
        Log.info("Getting item counter in the shopping cart");
        if (!field("cartBadge").exists()) {
            return 0;
        }

        return Integer.parseInt(field("cartBadge").getText());
    }
}
