package swaglabs.sidebar;

public class Sidebar {
    public enum NavLinks {
        ALL_ITEMS("All Items"),
        ABOUT("About"),
        LOGOUT("Logout"),
        RESET_APP_STATE("Reset App State");

        private String link;
        NavLinks(String link) {
            this.link = link;
        }

        public String getLinkName() {
            return link;
        }
    }
}
