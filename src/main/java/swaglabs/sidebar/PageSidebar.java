package swaglabs.sidebar;

import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

public class PageSidebar extends Page {
    public PageSidebar(ProjectApp app) {
        super(app);
    }

    public void navigateTo(Sidebar.NavLinks navLink) {
        String locator = getLocator("sidebarLink").replace("#link#", navLink.getLinkName());
        field(locator, USELOCATORASIS).click();
    }

    public void open() {
        field("sidebar").click();
    }

    public void close() {
        field("sidebarClose").click();
    }
}
