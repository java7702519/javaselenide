package swaglabs.login;

import com.codeborne.selenide.Condition;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

import static com.codeborne.selenide.Selenide.open;

public class PageLogin extends Page {
    public PageLogin(ProjectApp app) {
        super(app);
    }

    public void loginAs(String username, String password) {
        field("username").setValue(username);
        field("password").setValue(password);
        field("login").click();
    }

    public void openLoginPage() {
        open(getApp().getEnvironmentConfig("swagLabsUrl"));
        maximize();
    }

    public boolean hasErrorText() {
        return field("errorText").is(Condition.visible);
    }

    public String getErrorText() {
        return field("errorText").getText();
    }
}
