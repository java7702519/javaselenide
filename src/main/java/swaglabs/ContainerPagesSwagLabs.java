package swaglabs;

import common.kernel.containers.Container;
import common.kernel.core.ProjectApp;
import swaglabs.cart.PageCart;
import swaglabs.checkout.complete.PageComplete;
import swaglabs.checkout.overview.PageOverview;
import swaglabs.checkout.yourinfo.PageYourInfo;
import swaglabs.header.PageHeader;
import swaglabs.login.PageLogin;
import swaglabs.products.PageProducts;
import swaglabs.sidebar.PageSidebar;

public class ContainerPagesSwagLabs extends Container {
    public ContainerPagesSwagLabs(ProjectApp app) {
        super(app);
    }

    public PageLogin login = new PageLogin(app);
    public PageHeader header = new PageHeader(app);
    public PageProducts products = new PageProducts(app);
    public PageSidebar sidebar = new PageSidebar(app);
    public PageCart cart = new PageCart(app);
    public PageYourInfo yourInfo = new PageYourInfo(app);
    public PageOverview overview = new PageOverview(app);
    public PageComplete complete = new PageComplete(app);
}
