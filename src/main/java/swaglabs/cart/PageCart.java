package swaglabs.cart;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;
import swaglabs.products.Product;

import java.util.ArrayList;
import java.util.List;

public class PageCart extends Page {
    public PageCart(ProjectApp app) {
        super(app);
    }

    public void open() {
        field("cart").click();
    }

    public List<Product> getProducts() {
        Log.info("Getting products in the cart");
        List<Product> products = new ArrayList<>();

        ElementsCollection elements = fields("products");
        for (SelenideElement element : elements) {
            String productName = element.getText();
            String productPriceLocator = getLocator("productPrice").replace("#productName#", productName);
            String productDescLocator = getLocator("productDescription").replace("#productName#", productName);

            Product prod = new Product();
            prod.setName(productName);
            prod.setDescription(field(productDescLocator, USELOCATORASIS).getText());
            prod.setPrice(field(productPriceLocator, USELOCATORASIS).getText());
            products.add(prod);
        }

        return products;
    }

    public void checkout() {
        Log.info("Checking out the shopping cart");
        field("checkout").click();
    }
}
