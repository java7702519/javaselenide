package swaglabs.checkout.yourinfo;

import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

public class PageYourInfo extends Page {
    public PageYourInfo(ProjectApp app) {
        super(app);
    }

    public void enterFirstName(String value) {
        if (value == null || value.equals("")) {
            Log.info("Clearing field value");
            field("firstName").clear();
            return;
        }

        Log.info("Setting first name: " + value);
        field("firstName").setValue(value);
    }

    public void enterLastName(String value) {
        if (value == null || value.equals("")) {
            Log.info("Clearing field value");
            field("lastName").clear();
            return;
        }

        Log.info("Setting last name: " + value);
        field("lastName").setValue(value);
    }

    public void enterPostalCode(String value) {
        if (value == null || value.equals("")) {
            Log.info("Clearing field value");
            field("postalCode").clear();
            return;
        }

        Log.info("Setting postal code: " + value);
        field("postalCode").setValue(value);
    }

    public String getErrorMessage() {
        if (field("errorText").exists()) {
            return field("errorText").getText();
        }

        return null;
    }

    public void continueCheckout() {
        field("continue").click();
    }
}
