package swaglabs.checkout.complete;

import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

public class PageComplete extends Page {
    public PageComplete(ProjectApp app) {
        super(app);
    }

    public String getConfirmationText() {
        Log.info("Getting confirmation text");
        return field("confirmationText").getText();
    }
}
