package swaglabs.checkout.overview;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;
import swaglabs.products.Product;

import java.util.ArrayList;
import java.util.List;

public class PageOverview extends Page {
    public PageOverview(ProjectApp app) {
        super(app);
    }

    public List<Product> getProducts() {
        Log.info("Getting products in the overview page");
        List<Product> products = new ArrayList<>();

        ElementsCollection elements = fields("products");
        for (SelenideElement element : elements) {
            String productName = element.getText();
            String productPriceLocator = getLocator("productPrice").replace("#productName#", productName);
            String productDescLocator = getLocator("productDescription").replace("#productName#", productName);

            Product prod = new Product();
            prod.setName(productName);
            prod.setDescription(field(productDescLocator, USELOCATORASIS).getText());
            prod.setPrice(field(productPriceLocator, USELOCATORASIS).getText());
            products.add(prod);
        }

        return products;
    }

    public double getItemTotal() {
        Log.info("Getting item total");

        String value = field("itemSubTotal").getText().replace("Item total:", "").replace("$", "");
        return Double.parseDouble(value.trim());
    }

    public double getTax() {
        Log.info("Getting tax");

        String value = field("tax").getText().replace("Tax:", "").replace("$", "");
        return Double.parseDouble(value.trim());
    }

    public double getTotal() {
        Log.info("Getting total");

        String value = field("total").getText().replace("Total:", "").replace("$", "");
        return Double.parseDouble(value.trim());
    }

    public void finish() {
        field("finish").click();
    }
}
