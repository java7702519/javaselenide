package swaglabs.products;

public class ProductStates {
    public enum State {
        NOT_YET_ADDED("Add to cart"),
        ADDED("Remove");

        private String text;
        State(String text) {
            this.text = text;
        }

        public String getButtonText() {
            return text;
        }
    }
}
