package swaglabs.products;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.page.Page;

import java.util.ArrayList;
import java.util.List;

public class PageProducts extends Page {
    public PageProducts(ProjectApp app) {
        super(app);
    }

    public boolean isDisplayed() {
        field("appLogoText").shouldBe(Condition.visible);
        return field("appLogoText").isDisplayed() && field("pageTitle").getText().equals("Products");
    }

    public List<Product> getProducts() {
        Log.info("Getting products list");
        List<Product> products = new ArrayList<>();

        ElementsCollection elements = fields("products");
        for (SelenideElement element : elements) {
            String productName = element.getText();
            String productPriceLocator = getLocator("productPrice").replace("#productName#", productName);
            String productDescLocator = getLocator("productDescription").replace("#productName#", productName);

            Product prod = new Product();
            prod.setName(productName);
            prod.setDescription(field(productDescLocator, USELOCATORASIS).getText());
            prod.setPrice(field(productPriceLocator, USELOCATORASIS).getText());
            products.add(prod);
        }

        return products;
    }

    public void sortProducts(Sort.SortOptions sortOptions) {
        field("sortSelector").selectOptionContainingText(sortOptions.getText());
    }

    public int getCartSize() {
        Log.info("Getting item counter in the shopping cart");
        if (!field("shoppingCartBadge").exists()) {
            return 0;
        }

        return Integer.valueOf(field("shoppingCartBadge").getText());
    }

    public boolean addToCart(Product product) {
        if(!isAddedToCart(product)) {
            Log.info("Adding to cart: " + product.getName());
            String addToCartLocator = getLocator("productCartButton").replace("#productName#", product.getName());
            field(addToCartLocator, USELOCATORASIS).click();
            return true;
        }

        return false;
    }

    public boolean removeFromCart(Product product) {
        String addToCartLocator = getLocator("productCartButton").replace("#productName#", product.getName());

        if(isAddedToCart(product)) {
            Log.info("Removing from cart: " + product.getName());
            field(addToCartLocator, USELOCATORASIS).click();
            return true;
        }

        return false;
    }

    public ProductStates.State getState(Product product) {
        Log.info("Getting product state");
        String addToCartLocator = getLocator("productCartButton").replace("#productName#", product.getName());

        String text = field(addToCartLocator, USELOCATORASIS).getText();
        if (text.equals(ProductStates.State.ADDED.getButtonText())) {
            return ProductStates.State.ADDED;
        }

        return ProductStates.State.NOT_YET_ADDED;
    }

    public boolean isAddedToCart(Product product) {
        ProductStates.State state = getState(product);

        Log.info("Checking if product is added to cart");
        if(state.equals(ProductStates.State.ADDED)) {
            Log.info("Product is already added to cart: " + product.getName());
            return true;
        }

        Log.info("Product is not yet added to cart: " + product.getName());
        return false;
    }

    public void storeAddedProducts() {
        List<Product> products = getProducts();
        dataStore.put("addedProducts", products.stream().filter(this::isAddedToCart).toList());
    }
}
