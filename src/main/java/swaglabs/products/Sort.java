package swaglabs.products;

public class Sort {
    public enum SortOptions {
        NAME_A_TO_Z("az", "Name (A to Z)"),
        NAME_Z_TO_A("za", "Name (Z to A)"),
        PRICE_LOW_TO_HIGH("lohi", "Price (low to high)"),
        PRICE_HIGH_TO_LOW("hilo", "Price (high to low)");

        private String value;
        private String text;
        SortOptions(String value, String text) {
            this.value = value;
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public String getText() {
            return text;
        }
    }
}
