package swaglabs.products;

public class Product {
    private String name;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    private double price;
    public void setPrice(double price) {
        this.price = price;
    }
    public void setPrice(String price) {
        this.price = Double.parseDouble(price.trim().replaceAll("\\$|\\s|\\\"", ""));
    }
    public double getPrice() {
        return price;
    }

    private String description;
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public Product() {
        //Empty constructor
    }

    public Product(String name, double price, String description) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
}
