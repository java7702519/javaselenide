package definitions.steps;

import common.kernel.containers.Container;
import common.kernel.core.ProjectApp;
import common.kernel.step.Steps;
import definitions.containers.ContainerPages;
import steps.swaglabs.ContainerStepsSwagLabs;

public class StepsSwagLabs extends Steps {
    protected final ContainerStepsSwagLabs steps;
    protected final ContainerPages pages;

    public StepsSwagLabs(ProjectApp app, Container steps, Container pages) {
        super(app);
        this.steps = (ContainerStepsSwagLabs) steps;
        this.pages = (ContainerPages) pages;
    }
}
