package definitions.containers;

import common.aws.services.Cloudwatch;
import common.aws.services.Sqs;
import common.kernel.fileformats.Csv;

public class ContainerAws {
    public ContainerAws(Csv environment) {
        sqs = new Sqs(environment);
        cloudwatch = new Cloudwatch(environment);
    }

    public final Sqs sqs;
    public final Cloudwatch cloudwatch;
}
