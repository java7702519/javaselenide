package definitions.containers;

import common.kernel.containers.Container;
import common.kernel.containers.StepsContainer;
import common.kernel.core.ProjectApp;
import steps.swaglabs.ContainerStepsSwagLabs;

public class ContainerSteps extends StepsContainer {
    public ContainerSteps(ProjectApp app, Container pages) {
        super(app, pages);
    }

    public ContainerStepsSwagLabs swagLabs = new ContainerStepsSwagLabs(app, pages);
}
