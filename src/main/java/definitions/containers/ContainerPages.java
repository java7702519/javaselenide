package definitions.containers;

import common.kernel.containers.Container;
import common.kernel.core.ProjectApp;
import swaglabs.ContainerPagesSwagLabs;

public class ContainerPages extends Container {
    public ContainerPages(ProjectApp app) {
        super(app);
    }

    public ContainerPagesSwagLabs swagLabs = new ContainerPagesSwagLabs(app);
}
