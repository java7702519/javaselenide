package definitions.projects;

import common.kernel.core.CommonTest;
import definitions.containers.ContainerPages;
import definitions.containers.ContainerSteps;
import org.testng.annotations.BeforeMethod;

public class SwagLabsTest extends CommonTest {
    protected ContainerPages pages;
    protected ContainerSteps steps;

    @BeforeMethod
    public void beforeTest(Object[] data) {
        pages = new ContainerPages(app);
        steps = new ContainerSteps(app, pages);
    }
}
