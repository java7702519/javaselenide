package common.utils;

import com.github.javafaker.Faker;

public class Mocker {
    private static Faker faker;

    public static String getAddressNumber() {
        setFaker();
        return faker.address().streetAddressNumber();
    }

    public static String getStreet() {
        setFaker();
        return faker.address().streetName();
    }

    public static String getCity() {
        setFaker();
        return faker.address().cityName();
    }

    public static String getFirstName() {
        setFaker();
        return faker.name().firstName();
    }

    public static String getMiddleName() {
        setFaker();
        return faker.name().firstName();
    }

    public static String getLastName() {
        setFaker();
        return faker.name().lastName();
    }

    public static String getFullName() {
        setFaker();
        return faker.name().fullName();
    }

    public static String getEmailAddress() {
        return getEmailAddress(getFirstName(), getLastName());
    }

    public static String getEmailAddress(String firstname, String lastName) {
        return (firstname + "." + lastName + "@email.com").replace("[^A-Za-z0-9\\.\\@]", "");
    }

    public static int getRandomNumber(int min, int max) {
        setFaker();
        return faker.number().numberBetween(min, max);
    }

    public static long getRandomNumber(int length) {
        setFaker();
        return faker.number().randomNumber(length, true);
    }

    public static String getBankName() {
        setFaker();
        return faker.company().name();
    }

    public static void reset() {
        faker = null;
        setFaker();
    }

    private static void setFaker() {
        if (faker == null) {
            faker = new Faker();
        }
    }
}
