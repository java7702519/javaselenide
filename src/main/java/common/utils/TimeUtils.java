package common.utils;

import common.kernel.core.Log;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeUtils {
    /*
     * The default time stamp formatting when user did not specify any formatting
     */
    private static final String defaultFormat = TimeFormat.CUSTOMLONGDATE;

    public static String getTimeStamp() {
        return getTimeStamp(defaultFormat);
    }

    public static String getTimeStamp(String timeFormat) {
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(timeFormat);
        return format.format(stamp);
    }

    public static String offsetDays(String date, int offset) {
        return offsetDays(date, offset, defaultFormat);
    }

    public static String offsetDays(String date, int offset, String pattern) {
        LocalDateTime localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern)).atStartOfDay();
        LocalDateTime offsetDate;
        if (offset < 0) {
            offsetDate = localDate.minusDays(offset * -1);
        } else {
            offsetDate = localDate.plusDays(offset);
        }

        return offsetDate.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDateTime getTimerMark() {
        return LocalDateTime.now();
    }

    public static String getDuration(LocalDateTime start, LocalDateTime end) {
        return String.valueOf(getTimeLapse(start, end).toMinutes());
    }

    public static String getDuration(LocalDateTime start, LocalDateTime end, String format) {
        return DurationFormatUtils.formatDuration(getTimeLapse(start, end).toMillis(), format);
    }

    private static Duration getTimeLapse(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end);
    }

    public static String offsetMonths(String date, int offset) {
        return offsetMonths(date, offset, defaultFormat);
    }

    public static String offsetMonths(String date, int offset, String pattern) {
        LocalDateTime localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern)).atStartOfDay();
        LocalDateTime offsetDate;
        if (offset < 0) {
            offsetDate = localDate.minusMonths(offset * -1);
        } else {
            offsetDate = localDate.plusMonths(offset);
        }

        return offsetDate.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static String offsetYears(String date, int offset) {
        return offsetYears(date, offset, defaultFormat);
    }

    public static String offsetYears(String date, int offset, String pattern) {
        LocalDateTime localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern)).atStartOfDay();
        LocalDateTime offsetDate;
        if (offset < 0) {
            offsetDate = localDate.minusYears(offset * -1);
        } else {
            offsetDate = localDate.plusYears(offset);
        }

        return offsetDate.format(DateTimeFormatter.ofPattern(pattern));
    }

    public static String changeFormat(String date, String currentformat, String targetFormat) {
        LocalDateTime localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern(currentformat)).atStartOfDay();
        return localDate.format(DateTimeFormatter.ofPattern(targetFormat));
    }

    public static long getEpochOffset(int offsetSeconds) {
        Calendar cal = Calendar.getInstance();
        Date date = new Date();

        cal.setTime(date);
        cal.add(Calendar.SECOND, offsetSeconds);

        date.setTime(cal.getTime().getTime());
        return date.getTime();
    }

    public static void pause(int milliSeconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliSeconds);
        } catch (Exception e) {
            Log.warning("Pause interrupted", e);
        }
    }
}
