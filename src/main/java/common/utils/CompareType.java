package common.utils;

public class CompareType {
    public enum StringCompare {
        EQUALS,
        EQUALSIGNORECASE,
        CONTAINS,
        CONTAINSIGNORECASE
    }
}
