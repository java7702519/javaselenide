package common.utils;

public class TimeFormat {
    /**
     * <p>Formats the date  as MM/dd/yyyy</p>
     * <p>Ex: 01/01/1970</p>
     */
    public static final String SHORTDATE = "MM/dd/yyyy";

    /**
     * <p>Formats the date as MM/dd/yyyy HH:mm:ss</p>
     * <p>Ex: 01/01/1970 00:00:00</p>
     */
    public static final String LONGDATE = "MM/dd/yyyy HH:mm:ss";

    /**
     * <p>Formats the date as HH:mm:ss</p>
     * <p>Ex: 00:00:00</p>
     */
    public static final String TIME = "HH:mm:ss";

    /**
     * <p>Formats the date as HH:mm:ss</p>
     * <p>Ex: 00:00:00.00</p>
     */
    public static final String TIMEWITHMILLISECONDS = "H:mm:ss.SSS";

    /**
     * <p>Formats the date as yyyyMMddHHmmss</p>
     * <p>Ex: 19700101000000</p>
     */
    public static final String CUSTOMLONGDATE = "yyyyMMddHHmmssSS";

    /**
     * <p>Formats the date as yyyyMMdd</p>
     * <p>Ex: 19700101</p>
     */
    public static final String CUSTOMSHORTDATE = "yyyyMMdd";

    /**
     * <p>Formats the date as HHmmss</p>
     * <p>Ex: 000000</p>
     */
    public static final String CUSTOMTIME = "HHmmss";

    /**
     * <p>Formats the date as yyyy</p>
     * <p>Ex: 1970</p>
     */
    public static final String YEAR = "yyyy";

    /**
     * <p>Formats the date as M</p>
     * <p>Ex: 1</p>
     */
    public static final String MONTHINDEX = "M";

    /**
     * <p>Formats the date as MMM</p>
     * <p>Ex: Jan</p>
     */
    public static final String MONTHSHORTNAME = "MMM";

    /**
     * <p>Formats the date as MMMM</p>
     * <p>Ex: January</p>
     */
    public static final String MONTHLONGNAME = "MMMM";

    /**
     * <p>Formats the date as d</p>
     * <p>Ex: 1</p>
     */
    public static final String DATEOFMONTH = "d";

    /**
     * <p>Formats the date as E</p>
     * <p>Ex: Sun</p>
     */
    public static final String DAYOFWEEKSHORTNAME = "E";

    /**
     * <p>Formats the date as EEEE</p>
     * <p>Ex: Sunday</p>
     */
    public static final String DAYOFWEEKLONGNAME = "EEEE";
}
