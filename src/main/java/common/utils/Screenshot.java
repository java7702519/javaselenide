package common.utils;

import common.kernel.core.Log;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Map;
import java.util.function.Supplier;

public class Screenshot {
    private static final Map<String, Supplier<File>> captureMethods = Map.of(
            "single", Screenshot::captureSingleScreen,
            "multi", Screenshot::captureMultiscreen
    );

    private static File toFile(BufferedImage image) {
        String capturePath = System.getProperty("reporter.path") + System.getProperty("reporter.path.screenshots");
        String filename = capturePath + generateFilename();
        new File(capturePath).mkdirs();
        try {
            File file = new File(filename);
            ImageIO.write(image, "png", file);
            return file;
        } catch (Exception e) {
            Log.warning("Unable to save screenshot: " + filename, e);
        }

        return null;
    }

    private static String generateFilename() {
        return System.getProperty("reporter.path.screenshots.filename")
                .replaceAll("\\{environment\\}", System.getProperty("environment").toUpperCase())
                .replaceAll("\\{timestamp\\}", TimeUtils.getTimeStamp()) + ".png";
    }

    private static File captureSingleScreen() {
        try {
            Rectangle screen = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            return toFile(new Robot().createScreenCapture(screen));
        } catch (Exception e) {
            Log.warning("Failed to capture single screen image", e);
        }

        return null;
    }

    private static File captureMultiscreen() {
        try {
            GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] devices = graphics.getScreenDevices();

            Rectangle allScreen = new Rectangle();
            for (GraphicsDevice device : devices) {
                Rectangle screen = device.getDefaultConfiguration().getBounds();
                allScreen.width += screen.width;
                allScreen.height = Math.max(allScreen.height, screen.height);
            }

            return toFile(new Robot().createScreenCapture(allScreen));
        } catch (Exception e) {
            Log.warning("Failed to capture multi screen image", e);
        }

        return null;
    }

    public static File capture() {
        return captureMethods.get(System.getProperty("reporter.screenshots.mode")).get();
    }
}
