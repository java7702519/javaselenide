package common.utils;

import common.kernel.core.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static boolean hasMatch(String text, String pattern) {
        return getMatch(text, pattern).matches();
    }

    public static String getMatchingGroup(String text, String pattern,  int group) {
        Matcher matcher = getMatch(text, pattern);

        if (matcher.matches()) {
            return matcher.group(group);
        }

        Log.warning("Unable to get matching group: " + group);
        return "";
    }

    private static Matcher getMatch(String text, String pattern) {
        Log.info("Getting regular expression match. Pattern: " + pattern);
        Pattern p = Pattern.compile(pattern, Pattern.MULTILINE);
        return p.matcher(text);
    }
}
