package common.utils;

import common.kernel.core.Log;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class TestingFiles {
    private static String testFilePath = "src/test/resources/";

    public static String createAsFile(String filename, String data) {
        return createAsFile(filename, data, testFilePath);
    }

    public static String createAsFile(String filename, String data, String path) {
        try {
            File file = new File(path, filename);
            FileUtils.writeStringToFile(file, data, "UTF-8");

            String absolutePath = file.getAbsolutePath();
            Log.info("File created: " + absolutePath);
            return absolutePath;
        } catch (Exception e) {
            Log.fail("Failed to create file", e);
        }

        return null;
    }

    public static String readToString(String filename) {
        try {
            Log.info("Reading file: " + filename);
            return FileUtils.readFileToString(new File(filename), StandardCharsets.UTF_8);
        } catch (Exception e) {
            Log.fail("Failed to read file", e);
        }

        return null;
    }

    public static List<String> readToList(String filename) {
        try {
            Log.info("Reading file: " + filename);
            return FileUtils.readLines(new File(filename), StandardCharsets.UTF_8);
        } catch (Exception e) {
            Log.fail("Failed to read file", e);
        }

        return null;
    }

    public static void deleteFile(String filename) {
        if (FileUtils.deleteQuietly(new File(filename))) {
            Log.info("File deleted: " + filename);
            return;
        }

        Log.warning("Unable to delete file: " + filename);
    }

    public static void clean() {
        Log.info("Deleting test files: " + testFilePath + "*.*");
        FileUtils.deleteQuietly(new File(testFilePath));
    }
}
