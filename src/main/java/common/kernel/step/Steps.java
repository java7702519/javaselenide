package common.kernel.step;

import common.kernel.core.ProjectApp;

public abstract class Steps {
    protected final ProjectApp app;

    public Steps(ProjectApp app) {
        this.app = app;
    }
}
