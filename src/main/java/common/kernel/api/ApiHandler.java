package common.kernel.api;

import common.kernel.core.Log;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiHandler {
    private enum Auth {
        BASIC,
        OAUTH2,
        NONE
    }

    private Method method;
    private String url = "";
    private Auth auth = Auth.NONE;
    private String username = "";
    private String password = "";
    private String oauthToken = "";
    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> pathParams = new HashMap<>();
    private Map<String, String> formParams = new HashMap<>();
    private String body = "";
    private LinkedHashMap<String, Object> formDataParams = new LinkedHashMap<>();

    private void clearApiParam() {
        method = null;
        url = "";
        auth = Auth.NONE;
        username=  "";
        password = "";
        oauthToken = "";
        headers = new HashMap<>();
        pathParams = new HashMap<>();
        formParams = new HashMap<>();
        body = "";
        formDataParams = new LinkedHashMap<>();
    }

    public static class ApiBuilder {
        private ApiHandler rest;
        public ApiBuilder() {
            rest = new ApiHandler();
        }

        public ApiBuilder withBasicAuth(String username, String password) {
            rest.auth = Auth.BASIC;
            rest.username = username;
            rest.password = password;
            return this;
        }

        public ApiBuilder withOAuthToken(String value) {
            rest.auth = Auth.OAUTH2;
            rest.oauthToken = value;
            return this;
        }

        public ApiBuilder withMethod(Method value) {
            rest.method = value;
            return this;
        }

        public ApiBuilder withUrl(String value) {
            rest.url = value;
            return this;
        }

        public ApiBuilder withHeaders(String key, String value) {
            rest.headers.put(key, value);
            return this;
        }

        public ApiBuilder withHeaders(Map<String, String> value) {
            rest.headers.putAll(value);
            return this;
        }

        public ApiBuilder withPathParams(String key, String value) {
            rest.pathParams.put(key, value);
            return this;
        }

        public ApiBuilder withPathParams(Map<String, String> value) {
            rest.pathParams.putAll(value);
            return this;
        }

        public ApiBuilder withFormParams(String key, String value) {
            rest.formParams.put(key, value);
            return this;
        }

        public ApiBuilder withFormParams(Map<String, String> value) {
            rest.formParams.putAll(value);
            return this;
        }

        public ApiBuilder withBody(String value) {
            rest.body = value;
            return this;
        }

        public ApiBuilder withFormDataParams(String key, Object value) {
            rest.formDataParams.put(key, value);
            return this;
        }

        public ApiBuilder withFormDataParams(LinkedHashMap<String, Object> value) {
            rest.formDataParams.putAll(value);
            return this;
        }

        public Response execute() {
            RequestSpecification api = getRequestSpecification();
            logExecutionInfo();

            Response response = null;
            if (rest.method.equals(Method.GET)) {
                response = api.when().get(rest.url);
            } else if (rest.method.equals(Method.POST)) {
                response = api.when().post(rest.url);
            } else if (rest.method.equals(Method.PUT)) {
                response = api.when().put(rest.url);
            } else if (rest.method.equals(Method.PATCH)) {
                response = api.when().patch(rest.url);
            } else {
                Log.warning("Unsupported API method: " + rest.method.toString() + " " + rest.url);
            }

            rest.clearApiParam();
            return response;
        }

        private RequestSpecification getRequestSpecification() {
            RequestSpecification api = given();
            setHeaders(api);
            setPathParams(api);
            setFormParams(api);
            setBody(api);
            setFormData(api);
            //setMultiPart(api);
            setAuthentication(api);

            return api;
        }

        private void setHeaders(RequestSpecification api) {
            if (!rest.headers.isEmpty()) {
                api.headers(rest.headers);
            }
        }

        private void setPathParams(RequestSpecification api) {
            if (!rest.pathParams.isEmpty()) {
                api.pathParams(rest.pathParams);
            }
        }

        private void setFormParams(RequestSpecification api) {
            if (!rest.formParams.isEmpty()) {
                api.formParams(rest.formParams);
            }
        }

        private void setBody(RequestSpecification api) {
            if (!rest.body.equals("")) {
                api.body(rest.body);
            }
        }

        private void setFormData(RequestSpecification api) {
            if (!rest.formDataParams.isEmpty()) {
                rest.formDataParams.forEach((key, value) -> api.multiPart(key, value));
            }
        }

        private void setMultiPart(RequestSpecification api) {
            if (!rest.formDataParams.isEmpty()) {
                rest.formDataParams.forEach((key, value) -> {
                    if (key.equals("1")) {
                        api.multiPart(key, value, "file/csv");
                    } else {
                        api.multiPart(key, value);
                    }
                });
            }
        }

        private void setAuthentication(RequestSpecification api) {
            if (rest.auth.equals(Auth.BASIC)) {
                api.auth().basic(rest.username, rest.password);
            }

            if (rest.auth.equals(Auth.OAUTH2)) {
                api.auth().oauth2(rest.oauthToken);
            }
        }

        private String getUrl() {
            String url = rest.url;
            if (!rest.pathParams.isEmpty()) {
                for (String key : rest.pathParams.keySet()) {
                    url = url.replaceAll("\\{" + key + "\\}", rest.pathParams.get(key));
                }
            }

            return url;
        }

        private void logExecutionInfo() {
            StringBuilder builder = new StringBuilder();
            if (rest.auth.equals(Auth.BASIC)) {
                builder.append("\tBasic authentication: <username>:<password>" + System.lineSeparator());
            }

            if (rest.auth.equals(Auth.OAUTH2)) {
                builder.append("\tOAuth2 token: <access token>" + System.lineSeparator());
            }

            if (!rest.headers.isEmpty()) {
                builder.append("\tHeaders: " + rest.headers + System.lineSeparator());
            }

            if (!rest.formParams.isEmpty()) {
                builder.append("\tForm parameters: " + rest.formParams + System.lineSeparator());
            }

            if (!rest.formDataParams.isEmpty()) {
                builder.append("\tForm data: " + rest.formDataParams + System.lineSeparator());
            }

            if (!rest.body.equals("")) {
                builder.append("\tBody: " + rest.body + System.lineSeparator());
            }

            Log.info("Executing API " + rest.method.toString().toUpperCase() + " " + getUrl() + System.lineSeparator() + builder.toString().stripTrailing());
        }
    }
}
