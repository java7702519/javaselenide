package common.kernel.page;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.fileformats.Csv;
import common.robot.RobotHandler;
import common.utils.CompareType.StringCompare;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Page {
    private final String defaultDef;
    private final ProjectApp app;
    private final String pkg;
    private HashMap<String, Csv> customDef = new HashMap<>();

    protected final int USELOCATORFROMDEF = 0;
    protected final int USELOCATORASIS = 1;

    protected Page(ProjectApp app) {
        this.app = app;

        pkg = this.getClass().getPackageName().replaceAll("\\.", "//");
        defaultDef = this.getClass().getSimpleName() + ".elements";
        loadElementsDefinition(defaultDef);

        setComparators();
    }

    private HashMap<StringCompare, BiFunction<String, String, Boolean>> comparators = new HashMap<>();
    private void setComparators() {
        comparators.put(StringCompare.EQUALS, StringUtils::equals);
        comparators.put(StringCompare.EQUALSIGNORECASE, StringUtils::equalsIgnoreCase);
        comparators.put(StringCompare.CONTAINS, StringUtils::contains);
        comparators.put(StringCompare.CONTAINSIGNORECASE, StringUtils::containsIgnoreCase);
    }

    protected String getDataPath() {
        return Paths.get("").toAbsolutePath() + "/src/main/java/" + pkg + "/data/";
    }

    protected void loadElementsDefinition(String defFilename) {
        try {
            String defPath = (getDataPath() + defFilename + ".csv/").replace("\\","/");
            if (new File(defPath).exists()) {
                customDef.put(defFilename, new Csv(defPath, "element"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected SelenideElement field(String fieldElement) {
        return field(fieldElement, USELOCATORFROMDEF, defaultDef);
    }

    protected SelenideElement field(String fieldElement, String def) {
        return field(fieldElement, USELOCATORFROMDEF, def);
    }

    protected SelenideElement field(String fieldElement, int locatorType) {
        return field(fieldElement, locatorType, defaultDef);
    }

    protected void clickByActions(String fieldElement) {
        clickByActions(fieldElement, USELOCATORFROMDEF);
    }

    protected void clickByActions(String fieldElement, int locatorType) {
        Actions actions = new Actions(WebDriverRunner.getWebDriver());
        actions.moveToElement(field(fieldElement, locatorType)).click().perform();
    }

    protected void sendKeysByRobot(String text) {
        checkRobot();
        robot.sendKeys(text);
    }

    protected void sendKeysByRobot(int key) {
        checkRobot();
        robot.sendKeys(key);
    }

    private RobotHandler robot;
    private void checkRobot() {
        if (robot == null) {
            robot = new RobotHandler();
        }
    }

    private SelenideElement field(String fieldElement, int locatorType, String def) {
        String locator = fieldElement;
        if (locatorType == USELOCATORFROMDEF) {
            locator = getLocator(fieldElement, def);
        }

        Log.info("Interacting with field element Xpath: " + locator);
        return $x(locator);
    }

    protected ElementsCollection fields(String fieldElement) {
        return fields(fieldElement, USELOCATORFROMDEF, defaultDef);
    }

    protected ElementsCollection fields(String fieldElement, String def) {
        return fields(fieldElement, USELOCATORFROMDEF, def);
    }

    protected ElementsCollection fields(String fieldElement, int locatorType) {
        return fields(fieldElement, locatorType, defaultDef);
    }

    private ElementsCollection fields(String fieldElement, int locatorType, String def) {
        String locator = fieldElement;
        if (locatorType == USELOCATORFROMDEF) {
            locator = getLocator(fieldElement, def);
        }

        Log.info("Interacting with field element Xpath: " + locator);
        $$x(locator).should(CollectionCondition.sizeGreaterThan(0));
        return $$x(locator);
    }

    protected ProjectApp getApp() {
        return app;
    }

    protected String getLocator(String fieldElement) {
        return getLocator(fieldElement, defaultDef);
    }

    protected String getLocator(String fieldElement, String def) {
        Log.debug("Getting locator for field element: " + fieldElement + ". Definition file: " + def);
        return customDef.get(def).get(fieldElement, "xpath").replaceAll("\\'", "\"");
    }

    protected void maximize() {
        getWebDriver().manage().window().maximize();
    }

    protected void switchToLastWindow() {
        List<String> windows = getWindowHandles();
        getWebDriver().switchTo().window(windows.get(windows.size() - 1));
        Log.info("Switched to latest active window");
    }

    protected void switchToParentWindow() {
        getWebDriver().switchTo().window(getWindowHandles().get(0));
        Log.info("Switched to first active window");
    }

    protected void switchToWindowTitle(String title, StringCompare compareType) {
        List<String> windows = new ArrayList<>(getWebDriver().getWindowHandles());
        for (int ctr = 0 ; ctr < windows.size(); ctr++) {
            switchTo().window(windows.get(ctr));
            String winTitle = getWebDriver().getTitle();

            if (comparators.get(compareType).apply(title, winTitle)) {
                Log.info("Switched to window title: " + title);
                break;
            }

            if (ctr == windows.size() - 1) {
                Log.warning("Staying on current window. No window found with matching title: " + title);
            }
        }
    }

    private List<String> getWindowHandles() {
        return new ArrayList<>(getWebDriver().getWindowHandles());
    }

    protected final HashMap<String, Object> dataStore = new HashMap<>();
    public Object getDataStore(String key) {
        if (dataStore.containsKey(key)) {
            return dataStore.get(key);
        }

        Log.info("Returning empty string. Key not found in data store " + this.getClass().getSimpleName() + ". Key: " + key);
        return "";
    }
    public void setDataStore(String key, Object value) {
        dataStore.put(key, value);
    }
    public void clearDataStore() {
        dataStore.clear();
    }
}
