package common.kernel.reporters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.model.Log;
import com.aventstack.extentreports.model.Media;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import common.kernel.fileformats.FileAttachment;
import common.utils.Screenshot;
import common.utils.TestingFiles;
import common.utils.TimeFormat;
import common.utils.TimeUtils;

import java.io.File;
import java.util.List;

public class ExtentReportListener extends TestReporter {
    private ExtentReports extent;
    private ExtentSparkReporter spark;
    protected ExtentTest test;
    protected ExtentTest step;

    public void startSuite(String name) {
        extent = new ExtentReports();
        spark = new ExtentSparkReporter(System.getProperty("reporter.path") + "QAAuto_" + name + ".html");
        extent.attachReporter(spark);
        test = null;
        step = null;
    }

    public void startTest(String name) {
        test = extent.createTest(name);
    }

    public void step(String name) {
        step = test.createNode(name);
    }

    public void info(String message) {
        if (step != null) {
            step.info(message, getScreenshot());
        }
    }

    public void pass(String message) {
        if (step != null) {
            step.pass(message, getScreenshot());
        }
    }

    public void fail(String message) {
        if (step != null) {
            step.fail(message, getScreenshot());
        }
    }

    public void fail(String message, Throwable e) {
        if (step != null) {
            step.info("Test failed: " + message);
            step.fail(e, getScreenshot());
        }
    }

    public void warning(String message) {
        if (step != null) {
            step.warning(message, getScreenshot());
        }
    }

    public void warning(String message, Throwable e) {
        if (step != null) {
            step.warning(message);
            step.warning(e, getScreenshot());
        }
    }

    public void attach(FileAttachment.FileType type, String filename, String data) {
        List<Log> logs = step.getModel().getLogs();
        String details = step.getModel().getLogs().get(logs.size() - 1).getDetails();

        details = details + "<br>Open attachment: <a href='" + getFile(type, filename, data) + "'>" + filename + "</a>";
        step.getModel().getLogs().get(logs.size() - 1).setDetails(details);
    }

    public void endSuite() {
        extent.flush();
    }

    private String getFile(FileAttachment.FileType type, String filename, String data) {
        String path = System.getProperty("reporter.path") + System.getProperty("reporter.path.attachment");
        String absPath = TestingFiles.createAsFile(filename + "_" + TimeUtils.getTimeStamp(TimeFormat.CUSTOMLONGDATE) + type.getFileExtension(), data, path);

        return "./" + System.getProperty("reporter.path.attachment") + "/" + new File(absPath).getName();
    }

    private Media getScreenshot() {
        File screenshot = Screenshot.capture();
        String filename = "./" + System.getProperty("reporter.path.screenshots") + screenshot.getName();
        return MediaEntityBuilder.createScreenCaptureFromPath(filename).build();
    }
}
