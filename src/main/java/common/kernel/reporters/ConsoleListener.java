package common.kernel.reporters;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.*;

import java.nio.file.Paths;
import java.util.HashMap;

public class ConsoleListener extends TestReporter {
    private static HashMap<String, Level> logLevel = new HashMap<>();
    private static void initializeLogLevelMap() {
        logLevel.put("debug", Level.DEBUG);
        logLevel.put("info", Level.INFO);
        logLevel.put("warning", Level.WARN);
        logLevel.put("fail", Level.ERROR);
    }

    private Logger logger = LogManager.getLogger(Paths.get(System.getProperty("user.dir")).getFileName().toString());
    public void startSuite(String name) {
        logger.setAdditivity(false);
        PatternLayout layout = new PatternLayout(System.getProperty("reporter.console.layout"));

        logger.removeAllAppenders();
        logger.addAppender(new ConsoleAppender(layout));

        initializeLogLevelMap();
        logger.setLevel(logLevel.get(System.getProperty("logLevel").toLowerCase()));
    }

    public void debug(String message) {
        logger.debug(message);
    }

    public void info(String message) {
        logger.info(message);
    }

    public void warning(String message) {
        logger.warn(message);
    }

    public void warning(String message, Exception e) {
        logger.warn(message + System.lineSeparator() + ExceptionUtils.getStackTrace(e));
    }

    public void fail(String message) {
        logger.error(message);
    }

    public void fail(String message, Exception e) {
        logger.error(message, e);
    }

    public void startTest(String name) {
        logger.info("TEST: Test has started - " + name);
    }

    public void step(String name) {
        logger.info("STEP: " + name);
    }

    public void pass(String message) {
        logger.info("PASS: " + message);
    }

    public void endSuite() {
        logger.info("Test has ended");
    }
}
