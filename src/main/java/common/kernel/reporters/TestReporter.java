package common.kernel.reporters;

import common.kernel.fileformats.FileAttachment.FileType;

public abstract class TestReporter {
    public void startSuite(String name) {}
    public void step(String name) {}
    public void debug(String message) {}
    public void info(String message) {}
    public void warning(String message) {}
    public void warning(String message, Throwable e) {}
    public void pass(String message) {}
    public void fail(String message) {}
    public void fail(String message, Throwable e) {}
    public void startTest(String name) {}
    public void attach(FileType type, String filename, String data) {}
    public void endSuite() {}
}
