package common.kernel.core;

import common.utils.TestingFiles;
import common.utils.TimeFormat;
import common.utils.TimeUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Properties;

import static com.codeborne.selenide.Selenide.closeWebDriver;

class ProjectLoader {
    private static LocalDateTime start;
    private static LocalDateTime end;

    protected static void create(String name) {
        setDefaultProperties();

        Log.initialize();
        Log.startSuite(name);
        Log.info("Running test with the following properties: " + System.getProperties());

        SelenideLoader.load();
        Log.info("Selenide configurations are loaded");

        FileUtils.deleteQuietly(new File(System.getProperty("reporter.path")));

        start = TimeUtils.getTimerMark();
    }

    protected static void destroy() {
        Log.endTest();
        calculateRuntime();
        cleanProject();
    }

    private static void calculateRuntime() {
        end = TimeUtils.getTimerMark();
        String duration = TimeUtils.getDuration(start, end, TimeFormat.TIMEWITHMILLISECONDS);

        Log.info("Start time: " + start);
        Log.info("End time: " + end);
        Log.info("Duration (H:mm:ss.SSS): " + duration);
    }

    private static void cleanProject() {
        closeWebDriver();
    }

    protected static void cleanTest() {
        Log.info("Cleaning test execution");
        TestingFiles.clean();
        cleanProject();
        TimeUtils.pause(5000);
        Log.info("Test method is completed");
    }

    /**
     * Set default automation properties defined in default.properties.
     * Property can be overwritten by user through USER_HOME/Automation/automation.properties
     */
    private static void setDefaultProperties() {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("src/main/resources/default.properties"));
            for (Map.Entry<Object, Object> entry : props.entrySet()) {
                if (System.getProperty(entry.getKey().toString()) == null) {
                    System.setProperty(entry.getKey().toString().trim(), entry.getValue().toString().trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
