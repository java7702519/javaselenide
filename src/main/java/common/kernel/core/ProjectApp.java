package common.kernel.core;

import common.kernel.api.ApiHandler;
import common.kernel.fileformats.Csv;
import common.kernel.fileformats.Excel;

import java.nio.file.Paths;

public class ProjectApp {
    public ProjectApp() {
        String envPath = Paths.get("").toAbsolutePath() + "/src/main/java/data/env/demo.csv";
        environment = new Csv(envPath, "Name");
    }

    protected final Csv environment;

    public String getEnvironmentConfig(String key) {
        return environment.get(key, "Value");
    }

    public String getEnvironment() {
        return System.getProperty("environment").toLowerCase().replace(" ", "");
    }

    public final Excel excel = new Excel();
    public final ApiHandler.ApiBuilder api = new ApiHandler.ApiBuilder();
}
