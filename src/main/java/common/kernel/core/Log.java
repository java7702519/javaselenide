package common.kernel.core;

import common.kernel.fileformats.FileAttachment.FileType;
import common.kernel.reporters.ConsoleListener;
import common.kernel.reporters.ExtentReportListener;
import common.kernel.reporters.TestReporter;
import common.utils.TimeUtils;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Log {
    private static int logLevel;
    private static HashMap<String, Integer> levelMap;
    private static SoftAssert softAssert;

    protected static void initialize() {
        softAssert = new SoftAssert();

        createListeners();
        setLogLevel();
    }

    private static List<TestReporter> listeners = new ArrayList<>();
    private static void createListeners() {
        listeners = new ArrayList<>();
        listeners.add(new ExtentReportListener());
        listeners.add(new ConsoleListener());   //Add console to last
    }

    private static void setLogLevel() {
        levelMap = new HashMap<>();
        levelMap.put("debug", 0);
        levelMap.put("info", 1);
        levelMap.put("warn", 2);
        levelMap.put("error", 4);

        logLevel = levelMap.get(System.getProperty("logLevel").toLowerCase());
    }

    public static void startSuite(String name) {
        listeners.forEach(listener -> listener.startSuite(name));
    }

    public static void step(String name) {
        listeners.forEach(listener -> listener.step(name));
    }

    public static void debug(String message) {
        if (logLevel <= levelMap.get("debug")) {
            listeners.forEach(listener -> listener.debug(message));
        }
    }

    public static void info(String message) {
        if (logLevel <= levelMap.get("info")) {
            listeners.forEach(listener -> listener.info(message));
        }
    }

    public static void warning(String message) {
        if (logLevel <= levelMap.get("warn")) {
            listeners.forEach(listener -> listener.warning(message));
        }
    }

    public static void warning(String message, Exception e) {
        if (logLevel <= levelMap.get("warn")) {
            listeners.forEach(listener -> listener.warning(message, e));
        }
    }

    public static void pass(String message) {
        if (logLevel <= levelMap.get("info")) {
            listeners.forEach(listener -> listener.pass(message));
        }
    }

    public static void fail(String message) {
        if (logLevel <= levelMap.get("error")) {
            listeners.forEach(listener -> listener.fail(message));
        }

        ProjectLoader.destroy();
    }

    public static void fail(String message, Exception e) {
        if (logLevel <= levelMap.get("error")) {
            listeners.forEach(listener -> listener.fail(message, e));
        }

        ProjectLoader.destroy();
    }

    public static void skip(String message) {
        Log.info("Skipping the execution");
        throw new SkipException(message);
    }

    public static void startTest(String name) {
        listeners.forEach(listener -> listener.startTest(name));
    }

    //private static HashMap<String, String> fileMap;
    public static void attach(FileType fileType, String data) {
        attach(fileType, data, generateFilename(fileType));
    }

    public static void attach(FileType fileType, String data, String filename) {
        listeners.forEach(listener -> listener.attach(fileType, filename, data));
    }

    protected static void endTest() {
        listeners.forEach(TestReporter::endSuite);
        softAssert.assertAll();
    }

    public static void hardAssert(Object actual, Object expected, String errMsg) {
        hardAssert(actual, expected, errMsg, "Hard assert objects as equals");
    }

    public static void hardAssert(Object actual, Object expected, String errMsg, String msg) {
        Log.info(msg + " - Expected: [" + expected + "] | Actual: [" + actual + "]");
        try {
            Assert.assertEquals(actual, expected, errMsg);
        } catch (AssertionError err) {
            Log.fail(err.getMessage());
        }

        Log.info("Hard assertion passed");
    }

    public static void softAssert(Object actual, Object expected, String errMsg) {
        softAssert(actual, expected, errMsg, "Soft assert objects as equals");
    }

    public static void softAssert(Object actual, Object expected, String errMsg, String msg) {
        Log.info(msg + " - Expected: [" + expected + "] | Actual: [" + actual + "]");
        softAssert.assertEquals(actual, expected, errMsg);
    }

    public static void hardAssert(boolean condition, String errMsg) {
        hardAssert(condition, errMsg, "Hard assert condition as true");
    }

    public static void hardAssert(boolean condition, String errMsg, String msg) {
        Log.info(msg + " - Condition: [" + condition + "]");
        try {
            Assert.assertTrue(condition, errMsg);
        } catch (AssertionError err) {
            Log.fail(err.getMessage());
        }

        Log.info("Hard assertion passed");
    }

    public static void softAssert(boolean condition, String errMsg) {
        softAssert(condition, errMsg, "Soft assert condition as true");
    }

    public static void softAssert(boolean condition, String errMsg, String msg) {
        Log.info(msg + " - Condition: [" + condition + "]");
        softAssert.assertTrue(condition, errMsg);
    }

    public static void hardAssert(Collection<?> actual, Collection<?> expected, boolean orderBased, String errMsg) {
        hardAssert(actual, expected, orderBased, errMsg, "Hard assert collections as equal. Order based: " + orderBased);
    }

    public static void hardAssert(Collection<?> actual, Collection<?> expected, boolean orderBased, String errMsg, String msg) {
        Log.info(msg + " - Expected: [" + expected + "] | Actual: [" + actual + "]");
        if (orderBased) {
            Assert.assertEquals(actual, expected, errMsg);
        } else {
            Assert.assertEqualsNoOrder(actual, expected, errMsg);
        }

        Log.info("Hard assertion passed");
    }

    public static void softAssert(Collection<?> actual, Collection<?> expected, boolean orderBased, String errMsg) {
        softAssert(actual, expected, orderBased, errMsg, "Soft assert collections as equal. Order based: " + orderBased);
    }

    public static void softAssert(Collection<?> actual, Collection<?> expected, boolean orderBased, String errMsg, String msg) {
        Log.info(msg + " - Expected: [" + expected + "] | Actual: [" + actual + "]");
        if (orderBased) {
            softAssert.assertEquals(actual, expected, errMsg);
        } else {
            softAssert.assertEqualsNoOrder(actual.toArray(new Object[0]), expected.toArray(new Object[0]), errMsg);
        }
    }

    /*
    protected static void saveAttachments() {
        if (BooleanUtils.toBoolean(System.getProperty("reporter.attachment")) && !fileMap.isEmpty()) {
            String filename = "";
            try {
                new File(System.getProperty("reporter.path.attachment")).mkdirs();
                for (String name : fileMap.keySet()) {
                    filename = name;
                    FileUtils.writeStringToFile(new File(filename), fileMap.get(filename), Charset.forName("UTF-8"));
                }
            } catch (Exception e) {
                Log.warning("Unable to attach file: " + filename, e);
            } finally {
                fileMap = new HashMap<>();
            }
        }
    }

     */

    private static String generateFilename(FileType fileType) {
        return System.getProperty("reporter.path.attachment") + System.getProperty("reporter.path.attachment.filename")
                .replaceAll("\\{environment\\}", System.getProperty("environment").toUpperCase())
                .replaceAll("\\{timestamp\\}", TimeUtils.getTimeStamp()) + fileType.getFileExtension();
    }

    private static String generateFilename(String filename) {
        return System.getProperty("reporter.path.attachment") + filename;
    }
}
