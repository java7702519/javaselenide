package common.kernel.core;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;
import java.nio.file.Paths;

public abstract class Project {
    protected String getTestDirectory() {
        return (Paths.get("").toAbsolutePath() + "/src/test").replace("\\", "/");
    }

    @BeforeSuite
    public void beforeProject() {
        ProjectLoader.create(this.getClass().getName());
    }

   @BeforeMethod
    public void beforeMethod(Method method) {
        Log.startTest(method.getName());
    }

    @AfterMethod
    public void afterMethod() {
        ProjectLoader.cleanTest();
    }

    @AfterSuite
    public void afterProject() {
        ProjectLoader.destroy();
    }
}
