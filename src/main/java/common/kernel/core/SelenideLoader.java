package common.kernel.core;

import com.codeborne.selenide.Configuration;

class SelenideLoader {
    protected static void load() {
        Configuration.pageLoadTimeout = 180000;
        Configuration.timeout = 180000;
    }
}
