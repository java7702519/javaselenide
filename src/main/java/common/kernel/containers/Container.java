package common.kernel.containers;

import common.kernel.core.ProjectApp;

public abstract class Container {
    protected final ProjectApp app;

    protected Container(ProjectApp app) {
        this.app = app;
    }
}
