package common.kernel.containers;

import common.kernel.core.ProjectApp;

public abstract class StepsContainer extends Container {
    protected final Container pages;

    public StepsContainer(ProjectApp app, Container pages) {
        super(app);
        this.pages = pages;
    }
}
