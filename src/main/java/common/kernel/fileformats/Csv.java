package common.kernel.fileformats;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Map;

public class Csv {
    private JSONArray csvArray;
    private String rowRef = null;

    public Csv(String file) {
        try {
            this.csvArray = toJsonArray(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Csv(String file, String rowRef) {
        try {
            if (!new File(file).exists()) {
                throw new Exception();
            }

            this.csvArray = toJsonArray(file);
            this.rowRef = rowRef;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the equivalent CSV column value as specified by parameter col
     * for the given row value specified by parameter rowKey. Note that parameter
     * rowKey is checked under the CSV column rowRef.
     */
    public String get(String rowKey, String col) {
        return rowRef == null ? "" : get(rowRef, rowKey, col);
    }

    /**
     * Gets the equivalent CSV column value as specified by parameter col
     * for the given row value specified by parameter rowKey. Note that parameter
     * rowKey is checked under the CSV column rowRef.
     */
    public String get(String rowRef, String rowKey, String col) {
        for (Object obj : csvArray) {
            JSONObject json = (JSONObject) obj;
            if (json.has(rowRef) && json.getString(rowRef).equals(rowKey) && json.has(col)) {
                return json.getString(col);
            }
        }

        return "";
    }

    /**
     * Converts CSV file to JSONArray. Each row in the CSV file will be represented
     * as a JSONObject in the JSONArray.
     */
    private JSONArray toJsonArray(String file) {
        try {
            CsvSchema schema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            MappingIterator<Map<?, ?>> iterator = mapper.reader().forType(Map.class).with(schema).readValues(new File(file));
            List<Map<?, ?>> list  = iterator.readAll();
            return new JSONArray(list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONArray();
    }
}
