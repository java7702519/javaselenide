package common.kernel.fileformats;

public class FileAttachment {
    public enum FileType {
        CSV(".csv"),
        JSON(".json"),
        TXT(".txt"),
        XML(".xml");

        private final String fileExt;
        private FileType(String fileExt) {
            this.fileExt = fileExt;
        }

        public String getFileExtension() {
            return fileExt;
        }
    }
}
