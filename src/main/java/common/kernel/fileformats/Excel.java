package common.kernel.fileformats;

import common.kernel.core.Log;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class Excel {
    private final String XLS = "xls";
    private final String XLSX = "xlsx";
    private Workbook workbook;

    private String filePath;
    public void setFile(String value) {
        filePath = value;
        workbook = getWorkbook();
    }

    public Excel() {
        //Nothing
    }

    public Excel(String filePath) {
        this.filePath = filePath;
        workbook = getWorkbook();
    }

    private Workbook getWorkbook() {
        try {
            FileInputStream file = new FileInputStream(new File(filePath));
            Log.info("Opening Excel file: " + filePath);
            return FilenameUtils.getExtension(filePath).trim().toLowerCase().equals(XLS) ? new HSSFWorkbook(file) : new XSSFWorkbook(file);
        } catch (Exception e) {
            Log.warning("Unable to get Excel workbook", e);
        }

        return null;
    }

    private List<String> getHeaders(Row row) {
        List<String> headers = new ArrayList<>();
        for (int ctr = 0; ctr < row.getLastCellNum(); ctr++) {
            String value = getValueAsString(row.getCell(ctr), false);
            if (!value.equals("")) {
                headers.add(getValueAsString(row.getCell(ctr), false));
            }
        }

        return headers;
    }

    private String getValueAsString(Cell cell, boolean evaluateFormula) {
        DataFormatter formatter = new DataFormatter();

        if (evaluateFormula) {
            return formatter.formatCellValue(cell, workbook.getCreationHelper().createFormulaEvaluator());
        }

        return formatter.formatCellValue(cell);
    }

    private JSONObject getRowData(Row row, List<String> headers) {
        JSONObject rowData = new JSONObject();
        for (int col = 0; col < headers.size(); col++) {
            Cell cell = row.getCell(col);
            String value = "";  //Default for null cell and CellType.BLANK
            if (cell != null && !cell.getCellType().equals(CellType.BLANK)) {
                value = getValueAsString(cell, cell.getCellType().equals(CellType.FORMULA));
            }

            if (col == 0 && value.equals("")) {
                return new JSONObject();
            }

            rowData.put(headers.get(col), value);
        }

        return rowData;
    }

    /**
     * Gets worksheet data with headers as JSONArray. Each index of the JSONArray corresponds to one row
     * of the worksheet.
     */
    public JSONArray getWorkSheetData(String sheetName) {
        Sheet sheet = workbook.getSheet(sheetName);
        List<String> headers = new ArrayList<>();
        JSONArray rows = new JSONArray();

        for (int rowCtr = 0; rowCtr <= sheet.getLastRowNum(); rowCtr++) {
            Row row = sheet.getRow(rowCtr);
            if (rowCtr == 0) {
                headers = getHeaders(row);
                continue;
            }

            JSONObject rowData = getRowData(row, headers);
            if (!rowData.isEmpty()) {
                rows.put(rowData);
            }
        }

        return rows;
    }

    /**
     * Gets worksheet data of a worksheet given a specific row
     */
    public JSONObject getWorkSheetData(String sheetName, int rowNum) {
        JSONArray rows = getWorkSheetData(sheetName);
        return rows.getJSONObject(rowNum - 1);
    }

    /**
     * Gets workbook data as JSON. Take note that the method assumes that each worksheet
     * has headers at first row.
     */
    public JSONObject getWorkbookData() {
        JSONObject excelData = new JSONObject();
        for  (int sheetCtr = 0; sheetCtr < workbook.getNumberOfSheets(); sheetCtr++) {
            Sheet sheet = workbook.getSheetAt(sheetCtr);
            String sheetName = sheet.getSheetName();

            JSONArray sheetData = getWorkSheetData(sheetName);
            excelData.put(sheetName, sheetData);
        }

        return excelData;
    }

    /**
     * Gets workbook data as JSON. Take note that the method assumes that each worksheet
     * has headers at first row.
     */
    public JSONObject getWorkbookData(String filePath) {
        setFile(filePath);
        return getWorkbookData();
    }

    public boolean hasSheet(String sheet) {
        int count = workbook.getNumberOfSheets();
        for (int ctr = 0; ctr < count; ctr++) {
            if (workbook.getSheetName(ctr).equals(sheet)) {
                return true;
            }
        }

        return false;
    }
}
