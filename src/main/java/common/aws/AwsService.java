package common.aws;

import common.kernel.fileformats.Csv;

public class AwsService {
    protected final Csv envConfig;

    protected final Profile profile;
    public Profile getProfile() {
        return profile;
    }

    protected AwsService(Csv envConfig) {
        this.envConfig = envConfig;
        profile = new Profile(envConfig.get("awsAccountId", "Value"), envConfig.get("awsRegion", "Value"));
    }
}
