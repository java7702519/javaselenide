package common.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.regions.Regions;

public class Profile {
    private String accountId;
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getAccountId() {
        return accountId;
    }

    private Regions region;
    public void setRegion(Regions region) {
        this.region = region;
    }
    public void setRegion(String region) {
        this.region = Regions.valueOf(region.replace("-", "_").replace(" ", "").toUpperCase());
    }
    public Regions getRegion() {
        return region;
    }

    private AWSCredentialsProvider credentialsProvider;
    public AWSCredentialsProvider getCredentialsProvider() {
        return credentialsProvider;
    }
    private void setCredentialsProvider() {
        credentialsProvider = new SystemPropertiesCredentialsProvider();
    }

    public Profile(String accountId, Regions region) {
        this.accountId = accountId;
        this.region = region;
        setCredentialsProvider();
    }

    public Profile(String accountId, String region) {
        this.accountId = accountId;
        setRegion(region);
        setCredentialsProvider();
    }
}
