package common.aws.services;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.AWSLogsClientBuilder;
import com.amazonaws.services.logs.model.FilterLogEventsRequest;
import com.amazonaws.services.logs.model.FilterLogEventsResult;
import com.amazonaws.services.logs.model.FilteredLogEvent;
import common.aws.AwsService;
import common.kernel.core.Log;
import common.kernel.fileformats.Csv;
import common.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class Cloudwatch extends AwsService {
    public Cloudwatch(Csv envConfig) {
        super(envConfig);
    }

    private String logGroupName;
    public void setLogGroupName(String logGroupName) {
        this.logGroupName = logGroupName;
    }
    public String getLogGroupName() {
        return logGroupName;
    }

    private AmazonCloudWatch getCloudwatchClient() {
        return AmazonCloudWatchClientBuilder.standard()
                .withRegion(profile.getRegion())
                .withCredentials(profile.getCredentialsProvider())
                .build();
    }

    private AWSLogs getLogClient() {
        return AWSLogsClientBuilder.standard()
                .withRegion(profile.getRegion())
                .withCredentials(profile.getCredentialsProvider())
                .build();
    }

    public List<FilteredLogEvent> getFilteredLogs(String logGroupName, String filterPattern, int offsetMinutes) {
        Log.info("Setting up Cloudwatch log filter for event logs");
        FilterLogEventsRequest filter = new FilterLogEventsRequest()
                .withLogGroupName(logGroupName)
                .withLimit(100)
                .withStartTime(TimeUtils.getEpochOffset(offsetMinutes * 60))
                .withFilterPattern(filterPattern);

        boolean isEof = false;
        List<FilteredLogEvent> events = new ArrayList<>();

        Log.info("Searching log events with the filter request. Log group name: " + logGroupName + " | Start  time (minutes): " + offsetMinutes + " | Filter pattern: " + filterPattern);
        while (!isEof) {
            TimeUtils.pause(4000);
            FilterLogEventsResult result = getLogClient().filterLogEvents(filter);
            events.addAll(result.getEvents());

            if (result.getNextToken() == null || result.getNextToken().equals("")) {
                isEof = true;
                Log.info("End of log stream");
            } else {
                filter.setNextToken(result.getNextToken());
                Log.info("Setting token for the next log stream");
            }
        }

        Log.info("Log events found: " + events.size());
        return events;
    }

    public List<FilteredLogEvent> getFilteredLogs(String logGroupName, String filterPattern) {
        return getFilteredLogs(logGroupName, filterPattern, -60);
    }

    public List<FilteredLogEvent> getFilteredLogs(String filterPattern) {
        return getFilteredLogs(logGroupName, filterPattern);
    }
}
