package common.aws.services;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import common.aws.AwsService;
import common.kernel.fileformats.Csv;
import common.utils.TimeFormat;
import common.utils.TimeUtils;

public class Sqs extends AwsService {
    public Sqs(Csv envConfig) {
        super(envConfig);
    }

    private String queueName;
    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
    public String getQueueName() {
        return queueName;
    }

    public String getQueueUrl() {
        return "https://sqs." + profile.getRegion().getName() + ".amazonaws.com/" + profile.getAccountId() + "/" + queueName;
    }

    public SendMessageResult sendMessage(String message) {
        SendMessageRequest msgRequest = getMessageRequest(message);
        return getClient().sendMessage(msgRequest);
    }

    public SendMessageResult sendMessage(String queueName, String message) {
        setQueueName(queueName);
        return sendMessage(message);
    }

    private AmazonSQS getClient() {
        return AmazonSQSClientBuilder.standard()
                .withRegion(profile.getRegion())
                .withCredentials(profile.getCredentialsProvider())
                .build();
    }

    private SendMessageRequest getMessageRequest(String message) {
        String msgId = TimeUtils.getTimeStamp(TimeFormat.CUSTOMSHORTDATE) + TimeUtils.getTimeStamp(TimeFormat.CUSTOMTIME);

        SendMessageRequest msgRequest = new SendMessageRequest();
        msgRequest.setQueueUrl(getQueueUrl());
        msgRequest.setMessageGroupId(msgId + "1");
        msgRequest.setMessageDeduplicationId(msgId + "2");
        msgRequest.setMessageBody(message);

        return msgRequest;
    }
}
