package common.robot;

import common.kernel.core.Log;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class RobotHandler {
    private Robot robot;
    private int msDelay = 300;

    public RobotHandler() {
        try {
            robot = new Robot();
        } catch (Exception e) {
            Log.fail("Failed to initialize Robot", e);
        }
    }

    public void sendKeys(String text) {
        List<Integer> keyCodes = toKeyCodes(text);
        for (int keyCode : keyCodes) {
            if (keyCode == KeyEvent.VK_COLON) {
                sendKeysColon();
                continue;
            }

            robot.keyPress(keyCode);
            robot.delay(msDelay);
        }
    }

    public void sendKeys(int key) {
        robot.keyPress(key);
        robot.delay(msDelay);
    }

    private List<Integer> toKeyCodes(String text) {
        List<String> stringExplode = Arrays.asList(text.split("(?!^)"));
        List<IntStream> listStringChars = new ArrayList<>();
        stringExplode.forEach(i -> listStringChars.add(i.chars()));

        List<List<Integer>> listCharCodes = new ArrayList<>();
        listStringChars.forEach(i -> listCharCodes.add(i.boxed().toList()));

        List<Integer> listKeyCodes = new ArrayList<>();
        for (List<Integer> listCharCode : listCharCodes) {
            listCharCode.forEach(i -> listKeyCodes.add(KeyEvent.getExtendedKeyCodeForChar(i)));
        }

        return listKeyCodes;
    }

    private void sendKeysColon() {
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.keyPress(KeyEvent.VK_SEMICOLON);
        robot.keyRelease(KeyEvent.VK_SEMICOLON);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.delay(msDelay);
    }
}
