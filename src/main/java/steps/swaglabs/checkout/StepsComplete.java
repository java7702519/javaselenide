package steps.swaglabs.checkout;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;

public class StepsComplete extends StepsSwagLabs {
    public StepsComplete(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void verifyConfirmationText() {
        Log.step("Veriy confirmation text");

        String actual = pages.swagLabs.complete.getConfirmationText();
        String expected = "Your order has been dispatched, and will arrive just as fast as the pony can get there!";

        Log.hardAssert(actual, expected, "Incorrect confirmation text");
    }
}
