package steps.swaglabs.checkout;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;

public class StepsYourInfo extends StepsSwagLabs {
    public StepsYourInfo(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void verifyPageLoad() {
        Log.step("Check current page display");
        Log.hardAssert(pages.swagLabs.header.getCurrentPageTitle().equals("Checkout: Your Information"), "Incorrect page is displayed");
    }

    public void enterInfo(String username, String lastName, String postalCode) {
        Log.step("Enter user information");

        pages.swagLabs.yourInfo.enterFirstName(username);
        pages.swagLabs.yourInfo.setDataStore("username", username);

        pages.swagLabs.yourInfo.enterLastName(lastName);
        pages.swagLabs.yourInfo.setDataStore("lastName", lastName);

        pages.swagLabs.yourInfo.enterPostalCode(postalCode);
        pages.swagLabs.yourInfo.setDataStore("postalCode", postalCode);
    }

    public void verifyErrorMessage() {
        String errMsg = "Incorrect user info error message";

        if (String.valueOf(pages.swagLabs.yourInfo.getDataStore("username")).equals("")) {
            Log.softAssert(pages.swagLabs.yourInfo.getErrorMessage().equals("Error: First Name is required"), errMsg);
            return;
        }

        if (String.valueOf(pages.swagLabs.yourInfo.getDataStore("lastName")).equals("")) {
            Log.softAssert(pages.swagLabs.yourInfo.getErrorMessage().equals("Error: Last Name is required"), errMsg);
            return;
        }

        if (String.valueOf(pages.swagLabs.yourInfo.getDataStore("postalCode")).equals("")) {
            Log.softAssert(pages.swagLabs.yourInfo.getErrorMessage().equals("Error: Postal Code is required"), errMsg);
        }
    }

    public void continueCheckout() {
        Log.step("Continue checkout from your info page");
        pages.swagLabs.yourInfo.continueCheckout();
    }
}
