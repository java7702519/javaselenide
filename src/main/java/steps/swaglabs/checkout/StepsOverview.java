package steps.swaglabs.checkout;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;
import org.json.JSONObject;
import swaglabs.products.Product;

import java.util.ArrayList;
import java.util.List;

public class StepsOverview extends StepsSwagLabs {
    public StepsOverview(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    private List<Product> productsRef;
    private void getProducstRef() {
        if (productsRef == null || productsRef.size() == 0) {
            productsRef = (List<Product>) pages.swagLabs.products.getDataStore("addedProducts");
        }
    }
    public void verifyProducts() {
        Log.step("Verifying products in checkout overview");

        Log.info("Getting products in overview page");
        getProducstRef();
        List<Product> overviewProducts = pages.swagLabs.overview.getProducts();

        verifyProductCount(productsRef, overviewProducts);
        verifyProductNames(productsRef, overviewProducts);
        verifyProductPrice(productsRef, overviewProducts);
    }

    private double itemsTotal = 0;
    public void verifyItemsTotal() {
        getProducstRef();

        Log.info("Computing total price of items added for checkout");
        for (Product product : productsRef) {
            itemsTotal = itemsTotal + product.getPrice();
        }

        double overviewTotal = pages.swagLabs.overview.getItemTotal();

        Log.hardAssert(itemsTotal, overviewTotal, "Incorrect items subtotal");
    }

    public void verifyTotal() {
        double tax = pages.swagLabs.overview.getTax();
        double total = pages.swagLabs.overview.getTotal();

        Log.hardAssert(total, itemsTotal + tax, "Incorrect checkout total amount");
    }

    public void complete() {
        Log.step("Complete checkout");
        pages.swagLabs.overview.finish();
    }

    private void verifyProductCount(List<Product> productsRef, List<Product> productsActual) {
        Log.info("Verifying product count");
        Log.hardAssert(productsRef.size(), productsActual.size(), "Unexpected number of products in the checkout overview");
    }

    private void verifyProductNames(List<Product> productsRef, List<Product> productsActual) {
        Log.info("Verifying product list");
        List<Product> errProducts = new ArrayList<>();
        for (Product product : productsRef) {
            if (productsActual.stream().filter(prd -> product.getName().equals(prd.getName())).toList().size() == 0) {
                errProducts.add(product);
            }
        }

        Log.hardAssert(errProducts.size() == 0, "Products added by the user not reflecting in the checkout overview: " + errProducts);
    }

    private void verifyProductPrice(List<Product> productsRef, List<Product> productsActual) {
        Log.info("Verifying product price");
        JSONObject errProducts = new JSONObject();
        for (Product usrProduct : productsRef) {
            for (Product crtProduct : productsActual) {
                if ((usrProduct.getName().equals(crtProduct.getName())) && (usrProduct.getPrice() != crtProduct.getPrice())) {
                    JSONObject errPrice = new JSONObject();
                    errPrice.put("expected", usrProduct.getPrice());
                    errPrice.put("actual", crtProduct.getPrice());
                    errProducts.put(usrProduct.getName(), errPrice);
                } else if ((usrProduct.getName().equals(crtProduct.getName())) && (usrProduct.getPrice() == crtProduct.getPrice())) {
                    break;
                }
            }
        }

        Log.hardAssert(errProducts.isEmpty(), "Products added by the user has incorrect price in the checkout overview: " + errProducts);
    }
}
