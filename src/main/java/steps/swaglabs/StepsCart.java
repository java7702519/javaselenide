package steps.swaglabs;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;
import org.json.JSONObject;
import swaglabs.products.Product;

import java.util.ArrayList;
import java.util.List;

public class StepsCart extends StepsSwagLabs {
    public StepsCart(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void checkout() {
        Log.step("Check out the shopping cart");
        pages.swagLabs.cart.checkout();
    }

    public void verifyProducts() {
        Log.step("Veriying products added to the cart");

        Log.info("Getting products added by the user");
        List<Product> addedProducts = (List<Product>) pages.swagLabs.products.getDataStore("addedProducts");
        List<Product> cartProducts = pages.swagLabs.cart.getProducts();
        pages.swagLabs.cart.setDataStore("cartProducts", cartProducts);

        verifyProductCount(addedProducts, cartProducts);
        verifyProductNames(addedProducts, cartProducts);
        verifyProductPrice(addedProducts, cartProducts);
    }

    private void verifyProductCount(List<Product> addedProducts, List<Product> cartProducts) {
        Log.info("Verifying product count");
        Log.hardAssert(addedProducts.size(), cartProducts.size(), "Unexpected number of products in the cart");
    }

    private void verifyProductNames(List<Product> addedProducts, List<Product> cartProducts) {
        Log.info("Verifying product list");
        List<Product> errProducts = new ArrayList<>();
        for (Product product : addedProducts) {
            if (cartProducts.stream().filter(prd -> product.getName().equals(prd.getName())).toList().size() == 0) {
                errProducts.add(product);
            }
        }

        Log.hardAssert(errProducts.size() == 0, "Products added by the user not reflecting in the cart: " + errProducts);
    }

    private void verifyProductPrice(List<Product> addedProducts, List<Product> cartProducts) {
        Log.info("Verifying product price");
        JSONObject errProducts = new JSONObject();
        for (Product usrProduct : addedProducts) {
            for (Product crtProduct : cartProducts) {
                if ((usrProduct.getName().equals(crtProduct.getName())) && (usrProduct.getPrice() != crtProduct.getPrice())) {
                    JSONObject errPrice = new JSONObject();
                    errPrice.put("expected", usrProduct.getPrice());
                    errPrice.put("actual", crtProduct.getPrice());
                    errProducts.put(usrProduct.getName(), errPrice);
                } else if ((usrProduct.getName().equals(crtProduct.getName())) && (usrProduct.getPrice() == crtProduct.getPrice())) {
                    break;
                }
            }
        }

        Log.hardAssert(errProducts.isEmpty(), "Products added by the user has incorrect price in the cart: " + errProducts);
    }
}
