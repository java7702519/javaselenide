package steps.swaglabs.login;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import common.kernel.fileformats.Csv;
import definitions.steps.StepsSwagLabs;

public class StepsLogin extends StepsSwagLabs {
    public StepsLogin(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void as(String username, String password) {
        Log.step("Logging in to Swag Labs");
        Log.info("Opening Swag Labs");
        pages.swagLabs.login.openLoginPage();

        Log.info("Logging in as " + username + " | " + password);
        pages.swagLabs.login.loginAs(username, password);
    }

    public void verifyInvalidCredentials() {
        verifyErrorMessage("invalidCredentials");
    }

    public void verifyMissingUsername() {
        verifyErrorMessage("emptyUsername");
    }

    public void verifyMissingPassword() {
        verifyErrorMessage("emptyPassword");
    }

    public void verifySuccessfulLogin() {
        Log.step("Verifying successful login");
        Log.hardAssert(pages.swagLabs.products.isDisplayed(), "Unable to login successfully");
    }

    private void verifyErrorMessage(String errorType) {
        Log.step("Verifying: " + errorType);

        Log.info("Checking presence of error text");
        Log.hardAssert(pages.swagLabs.login.hasErrorText(), "Error text not found");

        Log.info("Verifying error message");
        Log.hardAssert(getErrorMessage(errorType), pages.swagLabs.login.getErrorText(), "Incorrect error message");
    }

    private Csv errorMessages;
    private String getErrorMessage(String errorType) {
        if (errorMessages == null) {
            errorMessages = new Csv("src/main/java/steps/swaglabs/login/data/StepsLogin.errorMessages.csv", "type");
        }

        return errorMessages.get(errorType, "message");
    }
}
