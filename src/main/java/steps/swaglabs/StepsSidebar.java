package steps.swaglabs;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;
import swaglabs.sidebar.Sidebar;

public class StepsSidebar extends StepsSwagLabs {
    public StepsSidebar(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void navigateTo(Sidebar.NavLinks navLink) {
        Log.step("Navigating to side bar: " + navLink.getLinkName());
        pages.swagLabs.sidebar.open();
        pages.swagLabs.sidebar.navigateTo(navLink);

        if (!navLink.equals(Sidebar.NavLinks.LOGOUT)) {
            pages.swagLabs.sidebar.close();
        }
    }
}
