package steps.swaglabs;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;

public class StepsHeader extends StepsSwagLabs {
    public StepsHeader(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    public void verifyPageDisplay(String pageTitle) {
        Log.step("Verify current page displayed");
        Log.hardAssert(pages.swagLabs.header.getCurrentPageTitle().equals(pageTitle), "Incorrect page is displayed");
    }

    public void openCart() {
        Log.step("Open cart");
        pages.swagLabs.header.openCart();
    }
}
