package steps.swaglabs;

import common.kernel.containers.Container;
import common.kernel.containers.StepsContainer;
import common.kernel.core.ProjectApp;
import steps.swaglabs.checkout.StepsComplete;
import steps.swaglabs.checkout.StepsOverview;
import steps.swaglabs.checkout.StepsYourInfo;
import steps.swaglabs.login.StepsLogin;

public class ContainerStepsSwagLabs extends StepsContainer {
    public ContainerStepsSwagLabs(ProjectApp app, Container pages) {
        super(app, pages);
    }

    public final StepsLogin login = new StepsLogin(app, this, pages);
    public final StepsHeader header = new StepsHeader(app, this, pages);
    public final StepsProducts products = new StepsProducts(app, this, pages);
    public final StepsSidebar sidebar = new StepsSidebar(app, this, pages);
    public final StepsCart cart = new StepsCart(app, this, pages);
    public final StepsYourInfo yourInfo = new StepsYourInfo(app, this, pages);
    public final StepsOverview overview = new StepsOverview(app, this, pages);
    public final StepsComplete complete=  new StepsComplete(app, this, pages);
}
