package steps.swaglabs;

import common.kernel.containers.Container;
import common.kernel.core.Log;
import common.kernel.core.ProjectApp;
import definitions.steps.StepsSwagLabs;
import swaglabs.products.Product;
import swaglabs.products.ProductStates;
import swaglabs.products.Sort;
import swaglabs.sidebar.Sidebar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StepsProducts extends StepsSwagLabs {
    public StepsProducts(ProjectApp app, Container steps, Container pages) {
        super(app, steps, pages);
    }

    private List<Product> productsRef;
    public void verifySorting(Sort.SortOptions sortOptions) {
        Log.step("Sorting products by: " + sortOptions.getText());

        getProductsReference();
        sortProducts(productsRef, sortOptions);
        Log.info("Product reference sorting: " + productsRef.stream().map(Product::getName).toList());

        pages.swagLabs.products.sortProducts(sortOptions);
        List<Product> productsActual = pages.swagLabs.products.getProducts();
        Log.info("Product actual sorting: " + productsRef.stream().map(Product::getName).toList());

        boolean isSorted = true;
        for (int ctr = 0; ctr < productsRef.size(); ctr++) {
            if (!productsRef.get(ctr).getName().equals(productsActual.get(ctr).getName())) {
                isSorted = false;
                break;
            }
        }

        Log.hardAssert(isSorted, "Incorrect product sorting. Sort options: " + sortOptions.getText());
    }

    public List<Product> addProductsToCart(int count) {
        Log.step("Adding products to cart. Target number of products to add: " + count);
        getProductsReference();

        int cartSize = pages.swagLabs.products.getCartSize();
        Log.info("Items count in cart before adding products: " + cartSize);

        List<Product> productsAdded = new ArrayList<>();
        for (Product product : productsRef) {
            boolean isAdded = pages.swagLabs.products.addToCart(product);
            if (isAdded) {
                productsAdded.add(product);
            }

            if (productsAdded.size() == count) {
                break;
            }
        }

        if (productsAdded.size() != count) {
            Log.warning("Unable to add the target number of products to cart. Products added: " + productsAdded.stream().map(Product::getName).toList());
        }

        pages.swagLabs.products.setDataStore("addedProducts", productsAdded);
        int addedCartSize = pages.swagLabs.products.getCartSize();
        Log.info("Items count in cart after adding products: " + addedCartSize);
        Log.softAssert(addedCartSize, cartSize + count, "Incorrect items count in adding to cart");

        return productsAdded;
    }

    public void removeProductsFromCart(List<Product> products) {
        Log.step("Removing products from cart: " + products.stream().map(Product::getName).toList());

        int cartSize = pages.swagLabs.products.getCartSize();
        Log.info("Items count in cart before removing products: " + cartSize);

        products.forEach(pages.swagLabs.products::removeFromCart);
        int removedCartSize = pages.swagLabs.products.getCartSize();
        Log.info("Items count in cart after removing products: " + removedCartSize);
        Log.softAssert(removedCartSize, cartSize - products.size(), "Incorrect items count in removing from cart");
    }

    public void verifyAddingAndRemovingToCart() {
        getProductsReference();

        List<Product> productsAdded = addProductsToCart(2);
        removeProductsFromCart(productsAdded);
    }

    public void verifyResetAppState() {
        steps.sidebar.navigateTo(Sidebar.NavLinks.RESET_APP_STATE);

        int cartSize = pages.swagLabs.products.getCartSize();
        Log.hardAssert(cartSize, 0, "Cart is not empty");

        getProductsReference();
        List<String> addedProducts = new ArrayList<>();
        for (Product product : productsRef) {
            if (pages.swagLabs.products.isAddedToCart(product)) {
                addedProducts.add(product.getName());
            }
        }
        Log.hardAssert(addedProducts.size() == 0, "Add to cart button state of the products are not reset: " + addedProducts);
    }

    public int getCartSize() {
        Log.step("Getting cart size");
        return pages.swagLabs.products.getCartSize();
    }

    public void verifyProductStateAs(Product product, ProductStates.State state) {
        Log.step("Verify product state. Product: " + product.getName() + " | State: " + state.name());
        Log.hardAssert(pages.swagLabs.products.getState(product).equals(state), "Incorrect product state");
    }

    private void getProductsReference() {
        if (productsRef == null || productsRef.size() == 0) {
            productsRef = pages.swagLabs.products.getProducts();
        }
    }

    private void sortProducts(List<Product> products, Sort.SortOptions sortOptions) {
        String activeSort = sortOptions.getValue();

        switch (activeSort) {
            case "az" -> products.sort(Comparator.comparing(Product::getName));
            case "za" -> {
                products.sort(Comparator.comparing(Product::getName));
                Collections.reverse(products);
            }
            case "lohi" -> products.sort(Comparator.comparing(Product::getPrice));
            case "hilo" -> {
                products.sort(Comparator.comparing(Product::getPrice));
                Collections.reverse(products);
            }
        }
    }
}
